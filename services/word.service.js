const PizZip = require('pizzip');
const Docxtemplater = require('docxtemplater');
const fs = require('fs');
const path = require('path');
const zip = require('node-zip')();

module.exports.createWordDocument = (replaceWords, newWords, inputFilePath) => {
  let content = fs
    .readFileSync(path.resolve(__dirname, inputFilePath), 'binary');
  let zip = new PizZip(content);
  let doc = new Docxtemplater();
  doc.loadZip(zip);
  let templateVariables = {};
  replaceWords.forEach((word, index) => {
      templateVariables[word] = newWords[index];
  });
  doc.setData(templateVariables);
  try {
      doc.render()
  }
  catch (error) {
    var e = {
        message: error.message,
        name: error.name,
        stack: error.stack,
        properties: error.properties,
    }
    console.log(JSON.stringify({error: e}));
    // throw error;
  }
  let buf = doc.getZip().generate({type: 'nodebuffer'});
  let filePath = '../output/' + newWords[0] + '_' + newWords[5] + '.docx';
  fs.writeFileSync(path.resolve(__dirname, filePath), buf);
}