const path = require('path');
const zip = require('node-zip')();
const fs = require('fs');
const uuidv1 = require('uuid/v1');

module.exports.generateZipFile = (files) => {
  files.forEach(file => {
      zip.file(file + '.docx', fs.readFileSync(path.join(__dirname, '../output/' + file + '.docx')));
  });
  const filename = uuidv1() + '.zip';
  const zipPath = path.join(__dirname, '../download/' + filename);
  let data = zip.generate({ base64:false, compression: 'DEFLATE' });
  fs.writeFileSync(zipPath, data, 'binary');
  return filename;
};