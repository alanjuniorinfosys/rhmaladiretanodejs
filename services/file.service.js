const fs = require('fs');
const path = require('path');

module.exports.removeAllFiles = (directory = 'output') => {
  fs.readdir(directory, (err, files) => {
    if (err) throw err;
  
    for (const file of files) {
      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
      });
    }
  });
}

module.exports.removeFile = (filePath) => {
  fs.unlink(path.resolve(__dirname, filePath), err => {
    if (err) throw err;
  });
}