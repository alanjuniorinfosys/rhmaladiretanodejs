const wordService = require('./word.service');
const zipService = require('./zip.service');
const fs = require('fs');
const path = require('path');

module.exports.generateZipFileFromCsvData = (header, body, wordPath) => {
  let csvResult = null;
  let replaceWords = header;
  let files = [];
  body.forEach(row => {
      let employeeInfo = row;
      wordService.createWordDocument(replaceWords, employeeInfo, wordPath);
      files.push(employeeInfo[0] + '_' + employeeInfo[5]);
  });
  return zipService.generateZipFile(files);
}

module.exports.readCsvAndGenerateZipFile = (csvPath, wordPath) => {
  let csvResult = null;
  let csvData = fs
    .readFileSync(path.resolve(__dirname, csvPath), {encoding: 'utf8'});
  let csvRows = csvData.split(/\r?\n/);
  let replaceWords = csvRows[0].split(';');
  csvRows.splice(0, 1); // remove csv header
  let csvBody = [];
  csvRows.forEach(row => csvBody.push(row.split(';')));
  const zipFileName = this.generateZipFileFromCsvData(replaceWords, csvBody, wordPath);
  return zipFileName;
}