const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const csvService = require('./services/csv.service');
const fileService = require('./services/file.service');
const wordService = require('./services/word.service');
const zipService = require('./services/zip.service');
const formidable = require('formidable');
const uuidv1 = require('uuid/v1');
const path = require('path');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', async function (req, res) {
    try {
        res.status(200).send({
            status: 'success',
            message: 'Application is live!'
        })
    } 
    catch (e) {
        res.status(400).send({
            status: 'error',
            data: {},
            error: e.message
        });
    }
});

app.get('/processDefaultFile', async function (req, res) {
    try {
        const csvPath = '../data.csv';
        const wordPath = '../input.docx';
        const outputFile = csvService.readCsvAndGenerateZipFile(csvPath, wordPath);
        fileService.removeAllFiles();
        res.status(200).send({
            status: 'success',
            message: outputFile
        });
    } 
    catch (e) {
        res.status(400).send({
            status: 'error',
            error: e.message
        });
    }
});

app.post('/proccessCvsFile/:csvPath/:wordPath', async function (req, res) {
    try {
        const wordPath = './../input/' + req.params.wordPath;
        const csvPath = './../input/' + req.params.csvPath;
        const outputFile = csvService.readCsvAndGenerateZipFile(csvPath, wordPath);
        fileService.removeAllFiles();
        fileService.removeFile(wordPath);
        fileService.removeFile(csvPath);
        res.status(200).send({
            status: 'success',
            message: outputFile
        });
    }
    catch (e) {
        res.status(400).send({
            status: 'error',
            data: {},
            error: e.message
        });
    }
})

app.post('/uploadfile', async function (req, res) {
    try {
        let form = new formidable.IncomingForm();
        const today = new Date();
        form.parse(req);
        let filename = '';
        form.on('fileBegin', function (name, file) {
            const rand = uuidv1();
            filename = rand + file.name;
            file.path = __dirname + '/input/' + rand + file.name;
        });
    
        form.on('file', function (name, file) {
            res.status(200).send({
                status: 'success',
                message: filename
            })
        });
    }
    catch (e) {
        res.status(400).send({
            status: 'error',
            data: {},
            error: e.message
        });
    }
});

app.post('/processCsvData', async function (req, res) {
    try {
        let body = req.body;
        if (body.header && body.body) {
            csvService.generateZipFileFromCsvData(body.header, body.body);
        }
        fileService.removeAllFiles();
        res.status(200).send({
            status: 'success',
            message: 'Finished'
        });
    }
    catch (e) {
        res.status(400).send({
            status: 'error',
            data: {},
            error: e.message
        });
    }
});

app.get('/download/:filename', async function (req, res) {
    try {
        res.download(__dirname + '/download/' + req.params.filename);
    }
    catch (e) {
        res.status(400).send({
            status: 'error',
            data: {},
            error: e.message
        });
    }
});

app.delete('/zip/:filename', async function (req, res) {
    try {
        fileService.removeFile(__dirname + '/download/' + req.params.filename);
        res.status(200).send({
            status: 'success',
            message: req.params.filename
        });
    }
    catch (e) {
        res.status(400).send({
            status: 'error',
            data: {},
            error: e.message
        });
    }
});

app.delete('/input/:filename', async function (req, res) {
    try {
        fileService.removeFile('./../input/' + req.params.filename);
        res.status(200).send({
            status: 'success',
            message: req.params.filename
        });
    }
    catch (e) {
        res.status(400).send({
            status: 'error',
            data: {},
            error: e.message
        });
    }
});

app.listen(3005, function () {
    console.log('Example app listening on port 3000!');
});